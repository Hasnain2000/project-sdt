﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectSDT.Models
{

    public class User
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public String Profile_image { get; set; }
        [Required]
        public String Name { get; set; }
        [Required]
        public String Email { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public String Password { get; set; }

        public String PhoneNo { get; set; }

        public String Address { get; set; }

        [Required]
        public int RoleId { get; set; }
        public virtual Role Role { get; set; }

        public int? CurrentPackage { get; set; }
        public virtual Package Package { get; set; }

        public String CurrentPackageStatus { get; set; }


        [DataType(DataType.Date)]
        public DateTime? PackageStartDate { get; set; }

        [DataType(DataType.Date)]
        public DateTime? PackageEndDate { get; set; }


        [Required]
        public String Status { get; set; }
    }
}
