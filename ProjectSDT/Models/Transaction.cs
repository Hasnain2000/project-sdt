﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectSDT.Models
{
    public class Transaction
    {

        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public int PackageId { get; set; }


        [Required]
        public int UserId { get; set; }


        private DateTime _date = DateTime.Now;
        [Required]
        [DataType(DataType.Date)]
        public DateTime Date { get { return _date; } set { _date = value; } }
    }
}
