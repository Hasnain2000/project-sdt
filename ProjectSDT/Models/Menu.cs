﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectSDT.Models
{
    public class Menu
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public String Name { get; set; }

        [Required]
        public String Monday { get; set; }


        [Required]
        public String Tuesday { get; set; }
        [Required]
        public String Wednesday { get; set; }
        [Required]
        public String Thursday { get; set; }
        [Required]
        public String Friday { get; set; }
        [Required]
        public String Saturday { get; set; }
        [Required]
        public String Sunday { get; set; }

    }
}
