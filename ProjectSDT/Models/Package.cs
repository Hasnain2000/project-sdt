﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectSDT.Models
{
    public class Package
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public String Name { get; set; }

        [Required]
        public String Description { get; set; }

        [Required]
        public Double Price { get; set; }

        [Required]
        public int MenuId { get; set; }

        [Required]
        public String Status { get; set; }
        
    }
}
