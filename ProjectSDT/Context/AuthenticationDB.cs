﻿using Microsoft.EntityFrameworkCore;
using ProjectSDT.Models;
using ProjectSDT.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectSDT.Context
{
    public class AuthenticationDB : DbContext
    {
        public AuthenticationDB(DbContextOptions<AuthenticationDB> options) : base(options)
        {
            SeedSomeData();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Role>().HasData(
                new { Id=1,Name="Admin" },
                new { Id = 2, Name = "User" }
                );

            modelBuilder.Entity<User>().HasData(
                new { Id=1, Name = "Admin",Email="admin@bunnybeans.com",Password=helpers.EncryptPassword("abc@123"), RoleId=1, Status="Active" }
                );
            base.OnModelCreating(modelBuilder);
        }



        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Package> Packages { get; set; }
        public DbSet<Newsletter> Newsletters { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<Menu> Menus { get; set; }
        public DbSet<MealDelivery> MealDeliverys { get; set; }
        public DbSet<Complain> Complains { get; set; }
        public DbSet<ChangePasswordRequest> ChangePasswordRequest { get; set; }
















        private void SeedSomeData()
        {


            IList<Role> Roles = new List<Role>();

                Roles.Add(new Role
                {
                    Id = 1,
                    Name = "Admin"
                });
                Roles.Add(new Role
                {
                    Id = 2,
                    Name = "User"
                });

            this.SaveChanges();
        }

        












    }
}
