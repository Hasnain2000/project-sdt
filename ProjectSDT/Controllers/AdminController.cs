﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProjectSDT.Context;
using ProjectSDT.Models;

namespace ProjectSDT.Controllers
{
    public class AdminController : Controller
    {
        private readonly AuthenticationDB _AuthenticationDBContext;


        public AdminController(AuthenticationDB AuthenticationDBContext)
        {
            this._AuthenticationDBContext = AuthenticationDBContext;
        }


        [Authorize(Roles = "Admin")]
        public IActionResult Index()
        {
            return RedirectToAction("Dashboard");
        }


        [Authorize(Roles = "Admin")]
        public IActionResult Dashboard()
        {
            return View();
        }

        [Authorize(Roles = "Admin")]
        public IActionResult Plans()
        {
            ViewData["MenuList"] = JsonSerializer.Serialize(_AuthenticationDBContext.Menus.ToList());
            ViewData["PackagesList"] = JsonSerializer.Serialize(_AuthenticationDBContext.Packages.ToArray());
            return View();
        }



        [Authorize(Roles = "Admin")]
        public IActionResult UpdateStatusPlans(int Id,String? status)
        {
            var plan = new Package { Id = Id };
            if (status=="Blocked")
            {
                plan.Status = "false";
            }
            else if (status == "Active")
            {
                plan.Status = "true";
            }
            _AuthenticationDBContext.Entry(plan).Property("Status").IsModified = true;
            _AuthenticationDBContext.SaveChanges();
            this.TempData["successMessage"] = "Plan Status Update SuccessFully";
            return RedirectToAction("Plans");
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public IActionResult AddPlans(Package model)
        {
            if (model.Name != null && model.Description != null && model.Price != null && model.MenuId != null && model.Status != null)
            {
                var newPackage = new Package()
                {
                    Name = model.Name,
                    Description = Uri.EscapeDataString(model.Description).ToString(),
                    Price = model.Price,
                    MenuId = model.MenuId,
                    Status = model.Status.ToString()
                };
                _AuthenticationDBContext.Packages.Add(newPackage);
                _AuthenticationDBContext.SaveChanges();
                this.TempData["successMessage"] = "Plan Added SuccessFully";
                return RedirectToAction("Plans");
            }
            else
            {
                this.TempData["ErrorMessage"] = "Please Fill all Fields ";
                return RedirectToAction("Plans");
            }

        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public String SinglePlans(int id)
        {
            return JsonSerializer.Serialize(_AuthenticationDBContext.Packages.Where(ct => ct.Id == id).First());

        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public IActionResult UpdatePlans(Package model)
        {
            if (model.Name != null && model.Description != null && model.Price != null && model.MenuId != null)
            {
                if (_AuthenticationDBContext.Packages.Find(model.Id) == null)
                {
                    this.TempData["ErrorMessage"] = "Error Index not Found";
                    return RedirectToAction("Plans");
                }
                Package UpdatedPackages = new Package()
                {
                    Id=model.Id,
                    Name = model.Name,
                    Description = Uri.EscapeDataString(model.Description).ToString(),
                    Price = model.Price,
                    MenuId = model.MenuId,
                    Status = _AuthenticationDBContext.Packages.Where(ct => ct.Id == model.Id).First().Status.ToString()
                };
                _AuthenticationDBContext.Entry(_AuthenticationDBContext.Packages.Find(model.Id)).CurrentValues.SetValues(UpdatedPackages);
                _AuthenticationDBContext.SaveChanges();
                this.TempData["successMessage"] = "Plan Updated SuccessFully";
                return RedirectToAction("Plans");
            }
            else
            {
                this.TempData["ErrorMessage"] = "Please Fill all Fields ";
                return RedirectToAction("Plans");
            }
        }


        [Authorize(Roles = "Admin")]
        public IActionResult DeletePlans(int? id)
        {
            if (_AuthenticationDBContext.Packages.Find(id) == null)
            {
                this.TempData["ErrorMessage"] = "Error Index not Found";
                return RedirectToAction("Menus");
            }

            _AuthenticationDBContext.Packages.Remove(_AuthenticationDBContext.Packages.Find(id));
            _AuthenticationDBContext.SaveChanges();
            this.TempData["successMessage"] = "Plan Deleted SuccessFully";
            return RedirectToAction("Plans");
        }











        [Authorize(Roles = "Admin")]
        public IActionResult Menus()
        {  
            ViewData["MenuList"] = JsonSerializer.Serialize(_AuthenticationDBContext.Menus.ToList());   
            return View();
        }

        [HttpPost][Authorize(Roles = "Admin")]
        public String SingleMenus(int id)
        {
            return JsonSerializer.Serialize(_AuthenticationDBContext.Menus.Where(ct => ct.Id == id).First());   
            
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public IActionResult AddMenus(Menu model)
        {
            if(model.Monday!=null && model.Tuesday != null && model.Wednesday != null && model.Thursday != null && model.Friday != null && model.Saturday != null && model.Sunday != null) 
            { 
                var newMenu = new Menu()
                {
                    Name = model.Name,
                    Monday = Uri.EscapeDataString(model.Monday).ToString(),
                    Tuesday = Uri.EscapeDataString(model.Tuesday).ToString(),
                    Wednesday = Uri.EscapeDataString(model.Wednesday).ToString(),
                    Thursday = Uri.EscapeDataString(model.Thursday).ToString(),
                    Friday = Uri.EscapeDataString(model.Friday).ToString(),
                    Saturday = Uri.EscapeDataString(model.Saturday).ToString(),
                    Sunday = Uri.EscapeDataString(model.Sunday).ToString(),
                };
                _AuthenticationDBContext.Menus.Add(newMenu);
                _AuthenticationDBContext.SaveChanges();
                this.TempData["successMessage"] = "Menu Added SuccessFully";
                return RedirectToAction("Menus");
            }
            else
            {
                this.TempData["ErrorMessage"] = "Please Fill all Fields ";
                return RedirectToAction("Menus");
            }
        }


        [HttpPost]
        [Authorize(Roles = "Admin")]
        public IActionResult UpdateMenus(Menu model)
        {
            if (model.Monday != null && model.Tuesday != null && model.Wednesday != null && model.Thursday != null && model.Friday != null && model.Saturday != null && model.Sunday != null)
            {
                if (_AuthenticationDBContext.Menus.Find(model.Id)==null)
                {
                    this.TempData["ErrorMessage"] = "Error Index not Found";
                    return RedirectToAction("Menus");
                }
                var UpdatedMenu = new Menu()
                {
                    Name = model.Name,
                    Monday = Uri.EscapeDataString(model.Monday).ToString(),
                    Tuesday = Uri.EscapeDataString(model.Tuesday).ToString(),
                    Wednesday = Uri.EscapeDataString(model.Wednesday).ToString(),
                    Thursday = Uri.EscapeDataString(model.Thursday).ToString(),
                    Friday = Uri.EscapeDataString(model.Friday).ToString(),
                    Saturday = Uri.EscapeDataString(model.Saturday).ToString(),
                    Sunday = Uri.EscapeDataString(model.Sunday).ToString(),
                };
                _AuthenticationDBContext.Entry(_AuthenticationDBContext.Menus.Find(model.Id)).CurrentValues.SetValues(model);
                _AuthenticationDBContext.SaveChanges();
                this.TempData["successMessage"] = "Menu Updated SuccessFully";
                return RedirectToAction("Menus");
            }
            else
            {
                this.TempData["ErrorMessage"] = "Please Fill all Fields ";
                return RedirectToAction("Menus");
            }
        }

        
        [Authorize(Roles = "Admin")]
        public IActionResult DeleteMenus(int? id)
        {
            if (_AuthenticationDBContext.Menus.Find(id) == null)
            {
                this.TempData["ErrorMessage"] = "Error Index not Found";
                return RedirectToAction("Menus");
            }
            if (_AuthenticationDBContext.Packages.Where(ct => ct.MenuId == id).Count() > 0)
            {
                this.TempData["ErrorMessage"] = "This Menu is associated with Plan. Please Remove Plan First";
                return RedirectToAction("Menus");
            }

            _AuthenticationDBContext.Menus.Remove(_AuthenticationDBContext.Menus.Find(id));
            _AuthenticationDBContext.SaveChanges();
            this.TempData["successMessage"] = "Menu Deleted SuccessFully";
            return RedirectToAction("Menus");
        }


        [Authorize(Roles = "Admin")]
        public IActionResult Customers()
        {
            ViewData["CustomersList"]=JsonSerializer.Serialize(_AuthenticationDBContext.Users.Where(ct => ct.RoleId == 2).ToArray());
            return View();
        }


        [Authorize(Roles = "Admin")]
        public IActionResult DeleteCustomer(int? id)
        {
            if (_AuthenticationDBContext.Users.Find(id) == null)
            {
                this.TempData["ErrorMessage"] = "Error Index not Found";
                return RedirectToAction("Customers");
            }
            if (_AuthenticationDBContext.Transactions.Where(ct => ct.UserId == id).Count()>0) {
                _AuthenticationDBContext.Transactions.Remove(_AuthenticationDBContext.Transactions.Where(ct => ct.UserId == id).First());
            }
            
            _AuthenticationDBContext.Users.Remove(_AuthenticationDBContext.Users.Find(id));
            _AuthenticationDBContext.SaveChanges();
            this.TempData["successMessage"] = "Customer and Associated Data is Deleted SuccessFully";
            return RedirectToAction("Customers");
        }

        
        [Authorize(Roles = "Admin")]
        public IActionResult UpdateCustomerStatus(int Id, String? status)
        {
            User user = new User { Id = Id };
            if (status == "Blocked")
            {
                user.Status = "Blocked";
            }
            else if (status == "Active")
            {
                user.Status = "Active";
            }
            _AuthenticationDBContext.Entry(user).Property("Status").IsModified = true;
            _AuthenticationDBContext.SaveChanges();
            this.TempData["successMessage"] = "User Status Updated SuccessFully";
            return RedirectToAction("Customers");
        }



        [Authorize(Roles = "Admin")]
        public IActionResult Complains()
        {
            return View();
        }


    }
}
