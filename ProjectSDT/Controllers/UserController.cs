﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProjectSDT.Context;

namespace ProjectSDT.Controllers
{
    public class UserController : Controller
    {
        private readonly AuthenticationDB _AuthenticationDBContext;
        public UserController(AuthenticationDB AuthenticationDBContext)
        {
            this._AuthenticationDBContext = AuthenticationDBContext;
        }

        [Authorize]
        public IActionResult Index()
        {
            return RedirectToAction("Login");
        }
        [Authorize(Roles = "User")]
        public IActionResult Dashboard()
        {
            var Email=User.Claims.Where(c => c.Type == ClaimTypes.Email).Select(c => c.Value).SingleOrDefault();
            var UserData=_AuthenticationDBContext.Users.Where(ct => ct.Email == Email).FirstOrDefault();
            ViewData["UserTransactionList"] = JsonSerializer.Serialize(_AuthenticationDBContext.Transactions.Where(ct => ct.UserId == UserData.Id).ToList());
            ViewData["UserPackageEndDate"] = UserData.PackageEndDate;
            ViewData["UserPackageStartDate"] = UserData.PackageStartDate;
            ViewData["UserCurrentPackageStatus"] = UserData.CurrentPackageStatus;
            if (UserData.CurrentPackage != null)
            {
                ViewData["UserPackageName"] = _AuthenticationDBContext.Packages.Where(ct => ct.Id == UserData.CurrentPackage).FirstOrDefault().Name;
            }else { ViewData["UserPackageName"] = ""; }
            
            return View();
        }
    }
}
