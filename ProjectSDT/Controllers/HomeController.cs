﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Protocols;
using ProjectSDT.Context;
using ProjectSDT.Models;

namespace ProjectSDT.Controllers
{
    public class HomeController : Controller
    {

        private readonly AuthenticationDB _AuthenticationDBContext;

        public HomeController(AuthenticationDB AuthenticationDBContext)
        {
            this._AuthenticationDBContext = AuthenticationDBContext;
        }


        public IActionResult Index()
        {

            return View();
        }

        
        public IActionResult Privacy()
        {
            return View();
        }


        public IActionResult Plans()
        {
            if (User.Identity.IsAuthenticated)
            {
                var Email = User.Claims.Where(c => c.Type == ClaimTypes.Email).Select(c => c.Value).SingleOrDefault();
                var user=_AuthenticationDBContext.Users.Where(ct => ct.Email == Email).FirstOrDefault();
                if (user.CurrentPackage != null)
                {
                    ViewData["UserPackage"] =user.CurrentPackage;
                }
                
            }
            ViewData["PackagesList"] = JsonSerializer.Serialize(_AuthenticationDBContext.Packages.ToArray());
            return View();
        }


        public IActionResult Menus()
        {
            ViewData["MenuList"] = JsonSerializer.Serialize(_AuthenticationDBContext.Menus.ToList());
            ViewData["PackagesList"] = JsonSerializer.Serialize(_AuthenticationDBContext.Packages.ToArray());
            return View();
        }


        public IActionResult Complain()
        {
            return View();
        }

        [HttpPost]
        public IActionResult SubmitComplain(Complain model)
        {
            var newComplain = new Complain()
            {
                Name = model.Name,
                Email=model.Email,
                Subject=model.Subject,
                Message=model.Message
            };
            _AuthenticationDBContext.Complains.Add(newComplain);
            _AuthenticationDBContext.SaveChanges();
            this.TempData["successMessage"] = "Thank You For Contacting Us. Our Support Team will Contact you as Soon as Possible.";
            return RedirectToAction("Complain");
        }


        [HttpPost]
        public IActionResult Newsletter(Newsletter model)
        {
            if(_AuthenticationDBContext.Newsletters.Where(ct => ct.Email == model.Email).Count()==0) { 
            var newLetter=new Newsletter()
            {
                Email = model.Email
            };
            _AuthenticationDBContext.Newsletters.Add(newLetter);
            _AuthenticationDBContext.SaveChanges();
                this.TempData["successMessage"] = "Thank You For Subscription.";
            }
            else
            {
                this.TempData["errorMessage"] = "Soory you are already Member of our Newletter.";
            }
            return Redirect("/#Newsletter");
        }




        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
