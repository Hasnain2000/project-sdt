﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting.Internal;
using ProjectSDT.Context;
using ProjectSDT.Helper;
using ProjectSDT.Models;

namespace ProjectSDT.Controllers
{
    public class AccountController : Controller
    {
        private readonly AuthenticationDB _AuthenticationDBContext;
        protected readonly IHostingEnvironment _hostingEnvironment;


        public AccountController(AuthenticationDB AuthenticationDBContext, IHostingEnvironment hostingEnvironment)
        {
            this._hostingEnvironment = hostingEnvironment;
            this._AuthenticationDBContext = AuthenticationDBContext;
        }

        public IActionResult Index()
        {
            return RedirectToAction("Login");
        }
        [Authorize]
        public IActionResult Dashboard()
        {
            if (User.Claims.Where(c => c.Type == ClaimTypes.Role).Select(c => c.Value).SingleOrDefault()=="Admin")
            {
                return RedirectToAction("Dashboard", "Plans");
            }
            else if (User.Claims.Where(c => c.Type == ClaimTypes.Role).Select(c => c.Value).SingleOrDefault() == "User")
            {
                return RedirectToAction("Dashboard", "User");
            }
            return RedirectToAction("Login");

        }



        [Authorize]
        public IActionResult Profile()
        {
            return View();
        }
        [Authorize][HttpPost]
        public IActionResult ChangeProfileImage(IFormFile file)
        {
            try
            {
                if (file != null && file.Length > 0)
                {
                    var uniqueFileName = Guid.NewGuid().ToString() + "_" + Path.GetFileName(file.FileName);
                    var uploads = Path.Combine(_hostingEnvironment.WebRootPath, "profile_image");
                    var filePath = Path.Combine(uploads, uniqueFileName);
                    file.CopyTo(new FileStream(filePath, FileMode.Create));
                    var UserEmail = User.Claims.Where(c => c.Type == ClaimTypes.Email).Select(c => c.Value).SingleOrDefault();
                    var UserData = _AuthenticationDBContext.Users.Where(ct => ct.Email == UserEmail).FirstOrDefault();

                    var UserUpdated = new User {
                        Id= UserData.Id,
                        Name=UserData.Name,
                        Email= UserData.Email,
                        PhoneNo= UserData.PhoneNo,
                        Password= helpers.EncryptPassword(UserData.Password),
                        Address= UserData.Address,
                        RoleId= UserData.RoleId,
                        CurrentPackage= UserData.CurrentPackage,
                        CurrentPackageStatus = UserData.CurrentPackageStatus,
                        PackageStartDate = UserData.PackageStartDate,
                        PackageEndDate = UserData.PackageEndDate,
                        Status=UserData.Status,
                        Profile_image = "/profile_image/" + uniqueFileName };
                    _AuthenticationDBContext.Entry(UserData).CurrentValues.SetValues(UserUpdated);
                    _AuthenticationDBContext.SaveChanges();
                    var Password=helpers.DecryptPassword(UserData.Password);
                    User ResignUser = new User
                    {
                        Email = UserEmail,
                        Password = Password
                    };
                    HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
                    return LoginFromProfile(ResignUser, "Profile", "Profile Image Update SuccessFully");
                }
                this.TempData["successMessage"] = "Profile Image Update SuccessFully";
                return RedirectToAction("Profile");
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Logfile cannot be read-only",ex);
            }

        }

        [Authorize]
        [HttpPost]
        public IActionResult UpdateProfileDetails(User model)
        {
            var UserEmail = User.Claims.Where(c => c.Type == ClaimTypes.Email).Select(c => c.Value).SingleOrDefault();
            var UserData = _AuthenticationDBContext.Users.Where(ct => ct.Email == UserEmail).FirstOrDefault();

            User UserUpdated = new User
            {
                Id = UserData.Id,
                Name = model.Name,
                Email = model.Email,
                PhoneNo = model.PhoneNo,
                Password = helpers.EncryptPassword(UserData.Password),
                Address = UserData.Address,
                RoleId = UserData.RoleId,
                CurrentPackage = UserData.CurrentPackage,
                CurrentPackageStatus = UserData.CurrentPackageStatus,
                PackageStartDate = UserData.PackageStartDate,
                PackageEndDate = UserData.PackageEndDate,
                Status = UserData.Status,
                Profile_image = UserData.Profile_image,
            };
            _AuthenticationDBContext.Entry(UserData).CurrentValues.SetValues(UserUpdated);
            _AuthenticationDBContext.SaveChanges();
            var Password = helpers.DecryptPassword(UserData.Password);
            User ResignUser = new User
            {
                Email = UserEmail,
                Password = Password
            };
            HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return LoginFromProfile(ResignUser, "Profile", "Profile Update SuccessFully");
        }

        [Authorize]
        public IActionResult ChangePassword()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public IActionResult ChangePassword(User model,String old_password)
        {
            if(helpers.PasswordStrength(model.Password) == null) { 
            var UserEmail = User.Claims.Where(c => c.Type == ClaimTypes.Email).Select(c => c.Value).SingleOrDefault();
            var UserData = _AuthenticationDBContext.Users.Where(ct => ct.Email == UserEmail).FirstOrDefault();

                
            User UserUpdated = new User
            {
                Id = UserData.Id,
                Name = UserData.Name,
                Email = UserData.Email,
                PhoneNo = UserData.PhoneNo,
                Password = helpers.EncryptPassword(model.Password),
                Address = UserData.Address,
                RoleId = UserData.RoleId,
                CurrentPackage = UserData.CurrentPackage,
                CurrentPackageStatus = UserData.CurrentPackageStatus,
                PackageStartDate = UserData.PackageStartDate,
                PackageEndDate = UserData.PackageEndDate,
                Status = UserData.Status,
                Profile_image = UserData.Profile_image,
            };
            _AuthenticationDBContext.Entry(UserData).CurrentValues.SetValues(UserUpdated);
            _AuthenticationDBContext.SaveChanges();
                

                this.ViewData["successMessage"] = "Password Changed Successfully";
                return View();
            }
            else
            {
                ModelState.AddModelError("Password", "Please Enter a Correct Password. Password Must Match");
                return View();
            }
        }


        public IActionResult Login()
        {
            if (User.Identity.IsAuthenticated){
                return RedirectToAction("Dashboard", "Account");
            }
            return View();
        }

        [HttpPost]
        public IActionResult Login(User model)
        {
            
            if (_AuthenticationDBContext.Users.Where(ct => ct.Email == model.Email).Count()>0 && helpers.DecryptPassword(_AuthenticationDBContext.Users.Where(ct => ct.Email == model.Email).FirstOrDefault().Password) ==model.Password)
            {
                var user=_AuthenticationDBContext.Users.Where(ct => ct.Email == model.Email).FirstOrDefault();
                if (user.Status=="Active")
                {
                    String Role = "Admin";
                    if (user.RoleId == 2)
                    {
                        Role = "User";
                    }
                    String profileImage=user.Profile_image;
                    if (profileImage == null)
                    {
                        profileImage = "https://ui-avatars.com/api/?name="+ user.Name;
                    }
                    String Address = user.Address;
                    String PhoneNo = user.PhoneNo;
                    if (Address == null) { Address = ""; }
                    if (PhoneNo == null) { PhoneNo = ""; }
                    var claims = new List<Claim>
                      {
                    new Claim(ClaimTypes.Uri,profileImage),
                    new Claim(ClaimTypes.Name,user.Name),
                    new Claim(ClaimTypes.Email,user.Email),
                    new Claim(ClaimTypes.Role,Role),
                    new Claim(ClaimTypes.StreetAddress,Address),
                    new Claim(ClaimTypes.MobilePhone,PhoneNo),

                      };
                    var identity = new ClaimsIdentity(
                        claims, CookieAuthenticationDefaults.AuthenticationScheme
                        );
                    var principal = new ClaimsPrincipal(identity);
                    var props = new AuthenticationProperties();
                    HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal, props).Wait();
                    return RedirectToAction("Index", "Home");
                }
                else if(user.Status == "Blocked")
                {
                    ModelState.AddModelError(string.Empty, "! Account Status Blocked. Please Contact Support for more Information.");
                    return View();
                }
               
            }

            ModelState.AddModelError(string.Empty, " ! Invalid Login Details");
            return View();

        }

        [HttpPost]
        public IActionResult LoginFromProfile(User model,String Path,String Message)
        {

            if (_AuthenticationDBContext.Users.Where(ct => ct.Email == model.Email).Count() > 0 && helpers.DecryptPassword(_AuthenticationDBContext.Users.Where(ct => ct.Email == model.Email).FirstOrDefault().Password) == model.Password)
            {
                var user = _AuthenticationDBContext.Users.Where(ct => ct.Email == model.Email).FirstOrDefault();
                if (user.Status == "Active")
                {
                    String Role = "Admin";
                    if (user.RoleId == 2)
                    {
                        Role = "User";
                    }
                    String profileImage = user.Profile_image;
                    if (profileImage == null)
                    {
                        profileImage = "https://ui-avatars.com/api/?name=" + user.Name;
                    }
                    String Address = user.Address;
                    String PhoneNo = user.PhoneNo;
                    if (Address == null) { Address = ""; }
                    if (PhoneNo == null) { PhoneNo = ""; }
                    var claims = new List<Claim>
                      {
                    new Claim(ClaimTypes.Uri,profileImage),
                    new Claim(ClaimTypes.Name,user.Name),
                    new Claim(ClaimTypes.Email,user.Email),
                    new Claim(ClaimTypes.Role,Role),
                    new Claim(ClaimTypes.StreetAddress,Address),
                    new Claim(ClaimTypes.MobilePhone,PhoneNo),

                      };
                    var identity = new ClaimsIdentity(
                        claims, CookieAuthenticationDefaults.AuthenticationScheme
                        );
                    var principal = new ClaimsPrincipal(identity);
                    var props = new AuthenticationProperties();
                    HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal, props).Wait();
                    this.ViewData["successMessage"] = Message;
                    return RedirectToAction(Path);
                }
                else if (user.Status == "Blocked")
                {
                    ModelState.AddModelError(string.Empty, "! Account Status Blocked. Please Contact Support for more Information.");
                    return View();
                }

            }

            ModelState.AddModelError(string.Empty, " ! Invalid Login Details");
            return View();

        }


        [HttpPost]
        public IActionResult Logout()
        {
            HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login");
        }




        public IActionResult Register()
        {

            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Dashboard", "Account");
            }
            return View();
        }


        [HttpPost]
        public IActionResult Register(User model)
        {
            if(helpers.ValidateEmailFormate(model.Email)==null && helpers.PasswordStrength(model.Password)==null)
            {
                var newUser = new User()
                {
                    Name = model.Name,
                    Email = model.Email,
                    Password =helpers.EncryptPassword(model.Password),
                    RoleId=2,
                    Status="Active"

                };
                _AuthenticationDBContext.Users.Add(newUser);
                _AuthenticationDBContext.SaveChanges();
                this.TempData["successMessage"] = "Thank for Registration. Login to access your Account";
                return RedirectToAction("Login");
            }
            else
            {
                
                if (helpers.ValidateEmailFormate(model.Email) != null)
                {
                    ModelState.AddModelError("Email", helpers.ValidateEmailFormate(model.Email));
                }
                else if(helpers.PasswordStrength(model.Password) != null)
                {
                    ModelState.AddModelError("Password", helpers.PasswordStrength(model.Password));
                }
                ModelState.AddModelError(string.Empty, " ! Please Fill Form Correctly.");
                
                
                return View();
            }

        }
















    }
}
